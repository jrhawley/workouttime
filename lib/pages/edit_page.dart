import 'dart:io';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:downloads_path_provider/downloads_path_provider.dart';
import '../models/workout.dart';
import '../models/exercise.dart';
import '../blocs/workouts_cubit.dart';
import '../blocs/workout_cubit.dart';
import '../states/workout_state.dart';
import '../components/edit_exercise.dart';
import '../helpers.dart';

class EditPage extends StatelessWidget {
  const EditPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) => WillPopScope(
      onWillPop: () => BlocProvider.of<WorkoutCubit>(context).goHome(),
      child: BlocBuilder<WorkoutCubit, WorkoutState>(builder: (context, state) {
        WorkoutEditing we = state as WorkoutEditing;
        return Scaffold(
            appBar: AppBar(
                leading: BackButton(
                    onPressed: () =>
                        BlocProvider.of<WorkoutCubit>(context).goHome()),
                title: InkWell(
                    onTap: () => showDialog(
                        context: context,
                        builder: (_) {
                          final controller =
                              TextEditingController(text: we.workout.title);
                          return AlertDialog(
                              content: TextField(
                                  controller: controller,
                                  decoration: InputDecoration(
                                      labelText: AppLocalizations.of(context)
                                          .workoutName)),
                              actions: [
                                TextButton(
                                    onPressed: () {
                                      if (controller.text.isNotEmpty) {
                                        Navigator.pop(context);
                                        Workout renamed = we.workout
                                            .copyWith(title: controller.text);
                                        BlocProvider.of<WorkoutsCubit>(context)
                                            .saveWorkout(renamed, we.index);
                                        BlocProvider.of<WorkoutCubit>(context)
                                            .editWorkout(renamed, we.index);
                                      }
                                    },
                                    child: Text(
                                        AppLocalizations.of(context).rename))
                              ]);
                        }),
                    child: Text(we.workout.title)),
                actions: [
                  IconButton(
                      icon: const Icon(Icons.file_download),
                      onPressed: () async {
                        Directory downloadsDir =
                            await DownloadsPathProvider.downloadsDirectory;
                        File('${downloadsDir.path}/${state.workout.title}.json')
                            .writeAsString(jsonEncode(state.workout.toJson()))
                            .then((_) => ScaffoldMessenger.of(context)
                                .showSnackBar(SnackBar(
                                    content: Text(AppLocalizations.of(context)
                                        .downloadWorkout))));
                      }),
                  IconButton(
                      icon: const Icon(Icons.delete),
                      onPressed: () {
                        BlocProvider.of<WorkoutsCubit>(context)
                            .deleteWorkout(state.workout);
                        BlocProvider.of<WorkoutCubit>(context).goHome();
                      })
                ]),
            body: ListView.builder(
                itemCount: state.workout.exercises.length,
                itemBuilder: (BuildContext context, int index) {
                  Exercise exercise = we.workout.exercises[index];
                  return (we.exIndex == index)
                      ? EditExercise(we.workout, we.index, we.exIndex)
                      : ListTile(
                          leading: Text(formatTime(exercise.prelude, true)),
                          title: Text(exercise.title),
                          trailing: Text(formatTime(exercise.duration, true)),
                          onTap: () => BlocProvider.of<WorkoutCubit>(context)
                              .editExercise(index));
                }),
            floatingActionButton: FloatingActionButton(
                onPressed: () {
                  state.workout.exercises
                      .add(const Exercise(prelude: 0, title: '', duration: 60));
                  BlocProvider.of<WorkoutCubit>(context)
                      .editExercise(we.workout.exercises.length - 1);
                },
                child: const Icon(Icons.add)));
      }));
}
