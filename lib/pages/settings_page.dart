import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../blocs/settings_cubit.dart';
import '../models/settings.dart';

class SettingsPage extends StatelessWidget {
  final _settingsCubit = SettingsCubit();

  SettingsPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(title: const Text("Workout Time! Settings")),
      body: BlocBuilder<SettingsCubit, Settings>(
          bloc: _settingsCubit,
          builder: (context, settings) => Container(
              padding: const EdgeInsets.all(15),
              child: Column(children: [
                Text(AppLocalizations.of(context).sounds,
                    style: const TextStyle(fontSize: 20)),
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text("3 seconds left"),
                      DropdownButton(
                          items: [
                            DropdownMenuItem(
                                value: 'none',
                                child: Text(AppLocalizations.of(context).none)),
                            DropdownMenuItem(
                                value: 'beep',
                                child: Text(AppLocalizations.of(context).beep)),
                            DropdownMenuItem(
                                value: 'ding',
                                child: Text(AppLocalizations.of(context).ding)),
                            DropdownMenuItem(
                                value: 'applause',
                                child:
                                    Text(AppLocalizations.of(context).applause))
                          ],
                          value: _settingsCubit.state.tickSound,
                          onChanged: (value) =>
                              _settingsCubit.setSound('tick', value))
                    ]),
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(AppLocalizations.of(context).endOfExercise),
                      DropdownButton(
                          items: [
                            DropdownMenuItem(
                                value: 'none',
                                child: Text(AppLocalizations.of(context).none)),
                            DropdownMenuItem(
                                value: 'beep',
                                child: Text(AppLocalizations.of(context).beep)),
                            DropdownMenuItem(
                                value: 'ding',
                                child: Text(AppLocalizations.of(context).ding)),
                            DropdownMenuItem(
                                value: 'applause',
                                child:
                                    Text(AppLocalizations.of(context).applause))
                          ],
                          value: _settingsCubit.state.exEndSound,
                          onChanged: (value) =>
                              _settingsCubit.setSound('exEnd', value))
                    ]),
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(AppLocalizations.of(context).endOfWorkout),
                      DropdownButton(
                          items: [
                            DropdownMenuItem(
                                value: 'none',
                                child: Text(AppLocalizations.of(context).none)),
                            DropdownMenuItem(
                                value: 'beep',
                                child: Text(AppLocalizations.of(context).beep)),
                            DropdownMenuItem(
                                value: 'ding',
                                child: Text(AppLocalizations.of(context).ding)),
                            DropdownMenuItem(
                                value: 'applause',
                                child:
                                    Text(AppLocalizations.of(context).applause))
                          ],
                          value: _settingsCubit.state.woEndSound,
                          onChanged: (value) =>
                              _settingsCubit.setSound('woEnd', value))
                    ])
              ]))));
}
