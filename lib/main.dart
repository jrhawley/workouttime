import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:path_provider/path_provider.dart';
import 'blocs/log_cubit.dart';
import 'pages/workout_page.dart';
import 'blocs/workout_cubit.dart';
import 'states/workout_state.dart';
import 'blocs/workouts_cubit.dart';
import 'pages/edit_page.dart';
import 'pages/home_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  final storage = await HydratedStorage.build(
    storageDirectory: await getApplicationDocumentsDirectory(),
  );
  HydratedBlocOverrides.runZoned(
    () => runApp(const WorkoutTime()),
    storage: storage,
  );
}

class WorkoutTime extends StatelessWidget {
  const WorkoutTime({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) => MaterialApp(
      title: 'Workout Time!',
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      theme: ThemeData(
          primarySwatch: Colors.blue,
          textTheme: const TextTheme(
              bodyText2: TextStyle(color: Color.fromARGB(255, 66, 74, 96)))),
      darkTheme: ThemeData(
          brightness: Brightness.dark,
          scaffoldBackgroundColor: Colors.black,
          colorScheme: ColorScheme.fromSwatch()
              .copyWith(secondary: Colors.blue, brightness: Brightness.dark),
          appBarTheme: const AppBarTheme(
              backgroundColor: Colors.blue, foregroundColor: Colors.white)),
      home: MultiBlocProvider(
          providers: [
            BlocProvider<WorkoutsCubit>(create: (BuildContext context) {
              WorkoutsCubit workoutsCubit = WorkoutsCubit();
              if (workoutsCubit.state.isEmpty) workoutsCubit.getWorkouts();
              return workoutsCubit;
            }),
            BlocProvider<WorkoutCubit>(
                create: (BuildContext context) => WorkoutCubit()),
            BlocProvider<LogCubit>(create: (BuildContext context) => LogCubit())
          ],
          child: BlocBuilder<WorkoutCubit, WorkoutState>(
              builder: (context, state) {
            if (state is WorkoutInitial) {
              return const HomePage();
            } else if (state is WorkoutEditing) {
              return const EditPage();
            } else {
              return const WorkoutPage();
            }
          })));
}
